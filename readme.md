UserEngine User Portal System

## Installation

- Install Composer (there is a specific windows build for windows users)
- Download the files to your directory of choice.
- Open up a command prompt window and move to that directory.
- Run the command "composer install" (requires git)
- Make sure your MySQL database is running.  By default, UserEngine will use the root user on a localhost machine.  This can be changed in app/config/database.php
- Create a userengine database.  Again, if you would rather change the name, you can do so in the app/config/database.php file.
- Run the command "php artisan migrate", followed by the command "php artisan db:seed".  This will add tables to your database and create some basic entries.
- Run the command "php artisan serve" to create a basic host that you can access the site from.
- Default accounts are admin and user, both with the password of "awesome."