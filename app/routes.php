<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Root Routes
Route::get( '/' , array( 'before' => 'login' , 'uses' => 'EngineController@mainPage' ) );
Route::post( '/' , array( 'before' => 'checkLoginPermission:1' , 'uses' => 'ActionController@userLogin' ) );
Route::get( 'controlPanel' , array( 'before' => 'auth', 'uses' => 'EngineController@userPage' ) );
Route::get( 'logout' , 'ActionController@doLogout' );

// Password Reset
Route::get( 'resetPassword' , array( 'before' => 'login' , 'uses' => 'RemindersController@getRemind' ) );
Route::post( 'resetPassword' , 'RemindersController@postRemind' );
Route::get( 'password/reset/{token}' , array( 'before' => 'login' , 'uses' => 'RemindersController@getReset' ) );
Route::post( 'password/reset/{token}' , 'RemindersController@postReset' );

//Registration
Route::get( 'register' , array( 'before' => 'login' , 'uses' => 'RegisterController@registerPage' ) );
Route::post( 'register' , array( 'before' => 'login' , 'uses' => 'RegisterController@doRegister' ) );

Route::group(array('before' => 'auth'), function(){
	Route::get( 'controlPanel/settings/plugin/{what}/{who?}' , function($what, $who=null) {
		$who=($who==null)?Auth::user():User::find($who);
		$plugin = Plugin::where('machineName', '=', $what)->first();

		if (!$plugin) {
			return Redirect::to( 'controlPanel/settings' );
		}

		if ($plugin->count()!=0) {
			if ($plugin->active && $plugin->hasSettings) {
				SkinElements::setUser($who);

				return App::make('Plugins\\'.$plugin->machineName.'\\PluginController' )->settingsController($plugin->machineName); // Note: This needs a default admin blade to load properly.
			}
		}

		return Redirect::to( 'controlPanel/settings' );
	} );
	Route::post( 'controlPanel/settings/plugin/{what}/{who?}' , function($what, $who=null) {
		$who=($who==null)?Auth::user():User::find($who);
		$plugin = Plugin::where('machineName', '=', $what)->first();

		if (!$plugin) {
			return Redirect::to( 'controlPanel/admin' );
		}
		
		if ($plugin->count()!=0) {
			if ($plugin->active && $plugin->hasSettings) {
				SkinElements::setUser($who);

				return App::make('Plugins\\'.$plugin->machineName.'\\PluginController' )->settingsPostController($plugin->machineName);
			}
		}
		return Redirect::to( 'controlPanel/admin' );
	} );

	// Default Pages
	Route::get( 'controlPanel/settings' , function(){return Redirect::to( 'controlPanel/settings/basic' );} );
	Route::get( 'controlPanel/settings/{subPage}' , 'EngineController@getGenericSettingsView' );


	// NOTE: Does this go here?
	Route::get( 'controlPanel/settings/{subPage}/{what}' , array( 'before' => 'checkforUserEditAdmin', 'uses' => 'AdminController@editUserSettings'  ) );
	Route::post( 'controlPanel/settings/admin/{what?}' , array( 'before' => 'checkforUserEditAdmin', 'uses' => 'AdminController@postAdminSettings'  ) );

	Route::group(array('before' => 'checkPermission:2'), function(){
		Route::get( 'controlPanel/admin' , function(){return Redirect::to( 'controlPanel/admin/info' );} );


		Route::group(array('before' => 'checkPermission:3'), function(){
			Route::get( 'controlPanel/admin/userMgmt', 'AdminController@getUserManagement' );
			Route::get( 'controlPanel/admin/confirmDelete/{what}',  'AdminController@confirmDelete' );
			Route::get( 'controlPanel/admin/removeUser/{what}',  'AdminController@removeUser' );
		});

		Route::group(array('before' => 'checkPermission:4'), function(){
			Route::get( 'controlPanel/admin/plugins', 'AdminController@getPluginsAdmin' );
			Route::get( 'controlPanel/admin/twiddlePlugin/{what}',  'AdminController@twiddlePlugin' );
			Route::get( 'controlPanel/admin/promote/{what}',  'AdminController@promoteLink' );
			Route::get( 'controlPanel/admin/demote/{what}',  'AdminController@demoteLink' );

			Route::get( 'controlPanel/admin/plugin/{what}' , function($what) {
				$plugin = Plugin::find($what);

				if (!$plugin) {
					return Redirect::to( 'controlPanel/admin' );
				}

				if ($plugin->count()!=0) {
					if ($plugin->active && $plugin->hasAdmin) {
						return App::make('Plugins\\'.$plugin->machineName.'\\PluginController' )->adminController($plugin->machineName); // Note: This needs a default admin blade to load properly.
					}
				}

				return Redirect::to( 'controlPanel/admin' );
			} );
			Route::post( 'controlPanel/admin/plugin/{what}' , function($what) {
				$plugin = Plugin::find($what);

				if (!$plugin) {
					return Redirect::to( 'controlPanel/admin' );
				}

				if ($plugin->count()!=0) {
					if ($plugin->active && $plugin->hasAdmin) {
						return App::make('Plugins\\'.$plugin->machineName.'\\PluginController' )->adminPostController($plugin->machineName);
					}
				}
				return Redirect::to( 'controlPanel/admin' );
			} );
		});

		Route::group(array('before' => 'checkPermission:5'), function(){
			Route::get( 'controlPanel/admin/roles/editRole/{what}' , 'AdminController@editRole' );
			Route::get( 'controlPanel/admin/roles/makeDefault/{what}' , 'AdminController@makeDefault' );

			Route::post( 'controlPanel/admin/roles/editRole/{what}' , 'AdminController@doEditRole' );
			Route::post( 'controlPanel/admin/roles/addRole' , 'AdminController@addRole' );
		});

		Route::group(array('before' => 'checkPermission:6'), function(){
			Route::get( 'controlPanel/admin/links' , 'AdminController@listLinks' );
			Route::get( 'controlPanel/admin/links/editLink/{what}' , 'AdminController@editLinks' );

			Route::post( 'controlPanel/admin/links' , 'AdminController@postEditLinks' );
		});

		Route::get( 'controlPanel/admin/{page}/{subPage?}' , 'AdminController@getGenericAdminView' );
		Route::post( 'controlPanel/admin/{what?}' , 'AdminController@postSiteSettings' );
	});

	Route::get( 'controlPanel/page/{what}', function($what) {
		if (Plugin::where('machineName', '=', $what)->count()!=0) {
			$plugin = Plugin::where('machineName', '=', $what)->first();
			if ($plugin->active && $plugin->hasView) {
				return App::make('Plugins\\'.$what.'\\PluginController' )->mainController($what);
			}
		}
		return Redirect::to( 'controlPanel' );
	});
	Route::post( 'controlPanel/page/{what}', function($what) {
		if (Plugin::where('machineName', '=', $what)->count()!=0) {
			$plugin = Plugin::where('machineName', '=', $what)->first();
			if ($plugin->active && $plugin->hasView) {
				return App::make('Plugins\\'.$what.'\\PluginController' )->mainPostController($what);
			}
		}
		return Redirect::to( 'controlPanel' );
	});
	Route::get( 'controlPanel/{page}/{action}/{what}' , function(){return Redirect::to( 'controlPanel' );} ); // Temp for now.

	Route::post( 'controlPanel/{page}/{action}/{what?}' , 'EngineController@processChanges' );
	Route::get( 'controlPanel/{page}/{subPage}' , 'EngineController@getGenericView' );
	Route::get( 'controlPanel/{page}' , 'EngineController@controlSwitch' );
});