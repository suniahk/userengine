<?php

class SiteSetting extends Eloquent {
	protected $table = 'siteSettings';

	public static function returnValue($key) {
		return SiteSetting::where('key', '=', $key)->first()->value;
	}
}