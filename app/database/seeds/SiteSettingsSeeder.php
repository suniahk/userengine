// app/database/seeds/UserTableSeeder.php

<?php

class SiteSettingsSeeder extends Seeder
{

	public function run()
	{
		DB::table('siteSettings')->delete();
		SiteSetting::create(array(
			'key'		=>	'siteName',
			'value'		=>	'UserEngine'
		));

		SiteSetting::create(array(
			'key'		=>	'adminRole',
			'value'		=>	'1'
		));

		SiteSetting::create(array(
			'key'		=>	'siteSkin',
			'value'		=>	'barebones'
		));
	}

}