// app/database/seeds/UserTableSeeder.php

<?php

class RolesSeeder extends Seeder
{

	public function run()
	{
		DB::table('Roles')->delete();
		Role::create(array(
			'name'		=>	'Administrator',
			'default'	=> 0,
			'special'	=> 1
		));

		Role::create(array(
			'name'		=>	'Member',
			'default'	=> 1,
			'special'	=> 1
		));

		Role::create(array(
			'name'		=>	'Banned',
			'default'	=> 0,
			'special'	=> 1
		));
	}

}