// app/database/seeds/UserTableSeeder.php

<?php

class PermissionsSeeder extends Seeder
{

	public function run()
	{
		DB::table('permissions')->delete();

		Permission::create(array(
			'description'		=>	'Allowed to Login',
			'roles'				=>	'1,2',
			'linkedPlugin'		=>	'0',
		));

		Permission::create(array(
			'description'		=>	'Access Admin Pages',
			'roles'				=>	'1',
			'linkedPlugin'		=>	'0',
		));

		Permission::create(array(
			'description'		=>	'Edit Other Users',
			'roles'				=>	'1',
			'linkedPlugin'		=>	'0',
		));

		Permission::create(array(
			'description'		=>	'Edit Plugins',
			'roles'				=>	'1',
			'linkedPlugin'		=>	'0',
		));

		Permission::create(array(
			'description'		=>	'Edit Roles',
			'roles'				=>	'1',
			'linkedPlugin'		=>	'0',
		));

		Permission::create(array(
			'description'		=>	'Edit Permissions',
			'roles'				=>	'1',
			'linkedPlugin'		=>	'0',
		));

		Permission::create(array(
			'description'		=>	'Edit Menu Links',
			'roles'				=>	'1',
			'linkedPlugin'		=>	'0',
		));
	}

}