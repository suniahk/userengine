// app/database/seeds/UserTableSeeder.php

<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();
		User::create(array(
			'name'     => 'Alex Watson',
			'username' => 'admin',
			'email'    => 'cleric.techie@gmail.com',
			'password' => Hash::make('awesome'),
			'roles'     => '1'
		));

		User::create(array(
			'name'     => 'Alex Watson',
			'username' => 'user',
			'email'    => 'cleric.techie@gmail.com',
			'password' => Hash::make('awesome'),
			'roles'     => '2'
		));
	}

}