<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PluginsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plugins', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('name', 32);
			$table->string('author', 64);
			$table->string('machineName', 32);
			$table->string('tableName', 32);
			$table->string('description', 128);
			$table->boolean( 'active' );
			$table->boolean( 'hasAdmin' );
			$table->boolean( 'hasView' );
			$table->boolean( 'hasSettings' );
			$table->integer('viewPermission');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plugins');
	}

}
