<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Skins extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skins', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('name', 32);
			$table->string('author', 64);
			$table->string('machineName', 32);
			$table->string('description', 128);
			$table->boolean( 'active' );
			$table->string('screenshot', 256);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skins');
	}

}
