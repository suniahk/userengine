<?php

class RegisterController extends BaseController {
	public function registerPage() {
		return View::make( 'user.register' );
	}

	public function doRegister() {
		return UserEngine::processRegistration();
	}
}