<?php

class EngineController extends BaseController {
	public function mainPage() {
		return View::make( 'display::login' );
	}

	public function userPage() {
		return View::make( 'userSwitch', array( 'page' => 'dashboard' ) );
	}

	public function controlSwitch($page) {
		return View::make( 'userSwitch', array( 'page' => 'display::controlPages.'.$page ) );
	}

	public function getPluginPage($subPage) {
		View::addNamespace( 'plugins' , './app/plugins/' . $subPage . '/views' ); // make this slightly more generalized later
		Return View::make( 'userSwitch', array( 'page' => 'plugins::'.$subPage , 'subPage' => $subPage ) );
	}

	public function getGenericView($page, $subPage) {
		Return View::make( 'userSwitch', array( 'page' => 'display::controlPages.'.$page , 'subPage' => $subPage ) );
	}

	public function getGenericSettingsView($subPage) {
		SkinElements::setUser(Auth::user());

		Return View::make( 'userSwitch', array( 'page' => 'settingsSwitch' , 'subPage' => $subPage, 'user' => Auth::user() ) );
	}

	public function ProcessChanges($page, $subPage, $which=null) {
		if ($page == "settings" && $subPage == "basic") {
			$redirect = UserEngine::processBasicSettingsChange();
		} else if ($page == "settings" && $subPage == "social") {
			$redirect = UserEngine::processSocialSettingsChange();
		} if (UserEngine::checkAdminAccess('admin') && $subPage == 'info' ) {
			$redirect = UserEngine::processSiteSettingsChange();
		}

		if (isset($redirect)) {
			return $redirect;
		}
		return Redirect::to( 'controlPanel/' . $page . '/' . $subPage );
	}
}