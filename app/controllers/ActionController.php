<?php

class ActionController extends BaseController {
	public function doLogout()
	{
		Auth::logout(); // log the user out of our application
		return Redirect::to('/'); // redirect the user to the login screen
	}

	public function userLogin() {
		// validate the info, create rules for the inputs
		$rules = array(
			'username'    => 'required', // make sure the email is an actual email
			'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);


		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('/')->withInput(Input::except('password'))->withErrors($validator); // send back the input (not the password) so that we can repopulate the form
		} else {
			// create our user data for the authentication
			$userdata = array(
				'username' 	=> Input::get('username'),
				'password' 	=> Input::get('password')
			);

			$remember=false;

			if (Input::get('submit')=='Login and Remember') {
				$remember = true;
			}

			if ( Auth::attempt( $userdata , $remember ) ) {
			    return Redirect::to( 'controlPanel' );
			} else {
				$errors = array(
					'auth' => 'The username or password you entered is incorrect.'
				);
				return Redirect::to( '/' )->withErrors( $errors );
			}
		}
		return Redirect::to( '/' );
	}
}