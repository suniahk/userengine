<?php

class AdminController extends BaseController {
	public function editUserSettings($action, $what) {
		SkinElements::setUser(User::find($what));

		return View::make( 'userSwitch', array( 'page' => 'settingsSwitch' , 'subPage' => $action ) );
	}

	public function listLinks() {
		return View::make( 'userSwitch', array( 'page' => 'adminSwitch' , 'subPage' => 'links' ) );
	}

	public function editLinks($what) {
		return View::make( 'userSwitch', array( 'page' => 'adminSwitch' , 'subPage' => 'linkForm', 'id' => Link::find($what) ) );
	}

	public function removeUser($what) {
		User::destroy($what);
		return Redirect::back()->with('success', 'The user was removed successfully.');
	}

	public function confirmDelete($what) {
		$user = User::find($what);
		return Redirect::back()->with('confirmDelete', array( 'user' => $user->username , 'id' => $what ));
	}

	public function postAdminSettings($what=null) {		
		return UserEngine::storeUserRoles();
	}

	public function postSiteSettings($what=null) {
		return UserEngine::storeSiteSettings();
	}

	public function twiddlePlugin($what) {
		$plugin = Plugin::find($what);
		$verb = '';
		$test='';

		if ($plugin->active) {
			$plugin->active = false;
			$verb = 'disabled';
			$link = Link::where('name','=',$plugin->name);
			if ($link->count()>0) {
				$link->first()->delete();
			}
		} else {
			$plugin->active = true;
			Plugins::createPluginTable($plugin->machineName, $plugin->tableName);
			
			if ($plugin->hasView) {
				$link = new Link();
				$link->name=$plugin->name;
				$link->machineName=$plugin->machineName;
				$link->type = "plugin";
				$link->priority = Link::all()->count() + 1;
				$link->url='controlPanel/page/'.$plugin->machineName;
				$link->location = 'main';

				$link->save();
			}
			
			$verb = 'activated';
		}

		$plugin->save();
		return Redirect::back()->with('success', 'The plugin ' . $plugin->name . ' was ' . $verb . ' successfully.');
	}

	public function promoteLink($what) {
		Links::changePriorityUp($what);
		return Redirect::back()->with('success', 'The link was promoted successfully.');
	}

	public function demoteLink($what) {
		Links::changePriorityDown($what);
		return Redirect::back()->with('success', 'The link was demoted successfully.');
	}

	public function getUserManagement($num=25) {
		return View::make( 'userSwitch', array( 'page' => 'adminSwitch' , 'subPage' => 'userMgmt' ) );
	}

	public function getGenericAdminView($page, $subPage=null) {
		if($subPage!=null) {
			Return View::make( 'userSwitch', array( 'page' => 'adminSwitch' , 'subPage' => $subPage ) );
		}

		Skinning::checkSavedSkins();

		Return View::make( 'userSwitch', array( 'page' => 'adminSwitch' , 'subPage' => $page ) );
	}

	public function getPluginsAdmin() {
		Plugins::checkSavedPlugins();
		return View::make( 'userSwitch', array( 'page' => 'adminSwitch' , 'subPage' => 'plugins' ) );
	}

	public function addRole() {
		$rules = array(
			'name'			=>	'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::back()
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::all()); // send back the input so that we can repopulate the form
		} else {
			$role = new Role();

			$role->name=Input::get('name');
			$role->permissions=0;

			$role->save();

			return Redirect::to( 'controlPanel/admin/roles' )->with('success', "The role " .$role->name. " was created successfully.");
		}
	}

	public function doEditRole($id) {
		$rules = array(
			'name'			=>	'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::back()
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::all()); // send back the input so that we can repopulate the form
		} else {
			$role = Role::find($id);

			$role->name=Input::get('name');

			$role->save();

			UserEngine::storeRolePermissions($id);

			return Redirect::to( 'controlPanel/admin/roles' )->with('success', "The role " .$role->name. " was changed successfully.");
		}
	}

	public function editRole($what) {
		return View::make( 'userSwitch', array( 'page' => 'adminSwitch' , 'subPage' => 'editRole', 'role' => Role::find($what)));
	}

	public function makeDefault($what) {
		$role = Role::find($what);
		$oldDefault= Role::where('default', "=", 1)->first();

		$oldDefault->default=0;
		$oldDefault->save();

		$role->default=1;
		$role->save();

		return Redirect::to( 'controlPanel/admin/roles' )->with('success', "The default role was changed successfully.");
	}
}