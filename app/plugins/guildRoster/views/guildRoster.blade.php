@section( 'pageName' )
Guild Roster
@stop

<div class="page-header">
	<h1>Guild Roster <span class='badge'>{{ count(UserEngine::usersWithRole('GUILDMEMBER')) }}</span></h1>
</div>

@if(count(UserEngine::usersWithRole('GUILDMEMBER'))==0)
	<div class="alert alert-danger">
		There are no guild members listed.
	</div>
@else
<table class='table'>
	<tr>
		<th>Username</th>
	</tr>
	@foreach(UserEngine::usersWithRole('GUILDMEMBER') as $member)
	<tr>
		<td>{{$member->username}}</td>
	</tr>
	@endforeach
</table>
@endif