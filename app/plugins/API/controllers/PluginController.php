<?php
namespace Plugins\API;

use Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Routing\Controller;
use View;

class PluginController extends Controller {
	public function mainController($subPage) {
		View::addNamespace( 'plugins' , './app/plugins/' . $subPage . '/views' );
		return View::make( 'userSwitch', array( 'page' => 'plugins::'.$subPage , 'subPage' => $subPage ) );
	}

	public function adminController($subPage) {
		View::addNamespace( 'plugins' , './app/plugins/' . $subPage . '/views' );
		return View::make( 'userSwitch', array( 'page' => 'adminSwitch', 'pluginPage' => 'plugins::'.$subPage , 'subPage' => 'plugin' ) );
	}

	public function settingsController($subPage) {
		View::addNamespace( 'plugins' , './app/plugins/' . $subPage . '/views' );
		return View::make( 'userSwitch', array( 'page' => 'settingsSwitch', 'pluginPage' => 'plugins::'.$subPage , 'subPage' => 'plugin' ) );
	}

	public static function createDatabase() {
		Schema::create('plugin_api', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('title', 32);
			$table->text('contents');

			$table->timestamps();
		});
	}
}