@section( 'pageName' )
UserEngine API
@stop

<div class="page-header">
	<h1>UserEngine Plugin API</h1>
</div>

<div class"page-header">
	<h2>Basic Plugin Notes</h2>
</div>

<ul>
	<li>Each plugin resides in its own folder in the app/plugins folder.</li>
	<li>In the root folder, there must be a plugin.xml file which holds the config information for your plugin.</li>
	<li>A basic plugin will have one sub-folder: views.  This is where your plugin view file is located.  This view file must be in the format '[machineName].blade.php'.  NOTE: [machineName] is defined in your plugin.xml.</li>
	<li>A more advanced plugin with have an admin view, as well as two additional sub-folders: models and controllers.</li>
</ul>

<div class"page-header">
	<h2>plugin.xml</h2>
</div>

<p>The following are all the xml tags which are available for plugins that UserEngine understands.  Place these in your plugin.xml file.</p>

<ul>
	<li><em>displayName</em>: The human-readable name of your plugin.  This is used for the page title.</li>
	<li><em>machineName</em>: The machine-readable name of your plugin.  No spaces allowed.  This is your page link and view name.</li>
	<li><em>description</em>: A short description that can be found on the plugins page.  Max of 128 characters.</li>
	<li><em>author</em>: The person who made the plugin.</li>
	<li><em>version</em>: The plugin version.</li>
</ul>