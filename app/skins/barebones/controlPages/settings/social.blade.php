@section( 'pageName' )
Social Settings
@stop

<div class="page-header">
	<h1>Social Account Settings {{ SkinElements::currentUsernameIfAdmin(); }}</h1>
</div>

	{{ SkinElements::newForm('form') }}
	{{ SkinElements::responseReport($errors) }}	

	<div class="panel panel-default">
 		<div class="panel-heading">Change Real Name</div>
 		<div class="panel-body">Enter what your publically viewable real name is here.</div>
		<div class="input-group">
			{{ SkinElements::realName( 'input-group-addon' , 'form-control' ) }}
		</div>
	</div>
	<div class="panel panel-default">
 		<div class="panel-heading">Accept Changes</div>
		<div class="panel-body">Enter your current password to make any changes.</div>
		<div class="input-group">
			{{ SkinElements::verifyPassword( 'input-group-addon' , 'form-control' ) }}
		</div>

		<div class='formButtonWrapper'>
			{{ SkinElements::submitButton( 'btn btn-primary' , true ) }}
		</div>
	</div>
{{ SkinElements::endForm() }}