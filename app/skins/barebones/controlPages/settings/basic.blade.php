@section( 'pageName' )
Basic Settings
@stop

<div class="page-header">
	<h1>Basic Account Settings {{ SkinElements::currentUsernameIfAdmin(); }}</h1>
</div>
	{{ SkinElements::newForm('form') }}
	
	<!-- if there are login errors, show them here -->
	{{ SkinElements::responseReport($errors) }}

	<div class="panel panel-default">
 		<div class="panel-heading">Change Email</div>
		<div class="input-group">
			{{ SkinElements::changeEmail( 'input-group-addon' , 'form-control' ) }}
		</div>
	</div>
	<div class="panel panel-default">
 		<div class="panel-heading">Change Password</div>
		<div class="panel-body">Leave this blank if you don't want to change your password</div>
		<div class="input-group">
			{{ SkinElements::newPassword( 'input-group-addon' , 'form-control' ) }}
		</div>
		<div class="input-group">
			{{ SkinElements::confirmNewPassword( 'input-group-addon' , 'form-control' ) }}
		</div>
	</div>
	<div class="panel panel-default">
 		<div class="panel-heading">Accept Changes</div>
		<div class="panel-body">Enter your current password to make any changes.</div>
		<div class="input-group">
			{{ SkinElements::verifyPassword( 'input-group-addon' , 'form-control' ) }}
		</div>

		<div class='formButtonWrapper'>
			{{ SkinElements::submitButton( 'btn btn-primary' , true ) }}
		</div>
	</div>
{{ SkinElements::endForm() }}