@section( 'pageName' )
Administrative Account Settings
@stop

<div class="page-header">
	<h1>Administrative Account Settings {{ SkinElements::currentUsernameIfAdmin(); }}</h1>
</div>

	{{ SkinElements::newForm('form') }}
	{{ SkinElements::responseReport($errors) }}	

	<div class="panel panel-default">
 		<div class="panel-heading">Change User Roles</div>
 		{{ SkinElements::listRoles( 'input-group' , 'form-control' , 'input-group-addon' ) }}
	</div>

	<div class='formButtonWrapper'>
		{{ SkinElements::submitButton( 'btn btn-primary' , true ) }}
	</div>
{{ SkinElements::endForm() }}