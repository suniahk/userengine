@section( 'pageName' )
Modify Link
@stop

<div class="page-header">
	<h1>Edit Link</h1>
</div>

{{ AdminElements::newForm('form') }}
	{{ AdminElements::responseReport($errors) }}	

	<div class="panel panel-default">
 		<div class="panel-heading">Link Info</div>
		<div class="input-group">
			<label for='text'>Link Text</label>
			<input type="text" id='text' name='text' />
		</div>
		<div class="input-group">
			<label for='url'>Link URL</label>
			<input type="text" id='url' name='url' />
		</div>
		<div class="input-group">
			<label for='location'>Link Location</label>
			<input type="text" id='location' name='location' />
		</div>
	</div>

		<div class='formButtonWrapper'>
			{{ AdminElements::submitButton( 'btn btn-primary' , "Save Link" ) }}
		</div>
	</div>
{{ Form::close() }}