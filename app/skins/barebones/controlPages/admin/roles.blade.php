@section( 'pageName' )
Roles
@stop

<div class="page-header">
	<h1>User Roles {{ AdminElements::newRoleButton("<span class='glyphicon glyphicon-plus'></span> New Role", "btn btn-success btn-sm pull-right") }}</h1>
</div>

{{ AdminElements::responseReport($errors) }}

{{ AdminElements::roleErrors( "alert alert-danger" ) }}

<table class='table'>
	{{ AdminElements::tabulateRoleHeaders() }}
	{{ AdminElements::tabulateUserRoles('btn btn-primary btn-sm' , "btn btn-danger btn-sm") }}
</table>