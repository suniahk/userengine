@section( 'pageName' )
Site Info
@stop

<div class="page-header">
	<h1>Basic Website Information</h1>
</div>

{{ AdminElements::newForm('form') }}
	{{ AdminElements::responseReport($errors) }}

	<div class="panel panel-default">
 		<div class="panel-heading">Change Website Name</div>
		<div class="input-group">
			{{ AdminElements::changeSiteName('input-group-addon', 'form-control') }}
		</div>
	</div>

	<div class="panel panel-default">
 		<div class="panel-heading">Change Website Skin</div>
		<div class="input-group">
			{{ AdminElements::changeSiteSkin('input-group-addon', 'form-control') }}
		</div>
	</div>

		<div class='formButtonWrapper'>
			{{ AdminElements::submitButton( 'btn btn-primary' , "Save Changes" ) }}
		</div>
	</div>
{{ AdminElements::endForm() }}