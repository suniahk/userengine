@section( 'pageName' )
User Management
@stop

<div class="page-header">
	<h1>User Management <span class='badge'>{{ count(User::all()) }}</span></h1>
</div>

{{ AdminElements::responseReport($errors) }}

{{ AdminElements::confirmDeleteUser("alert alert-warning", "btn btn-primary", "btn btn-danger") }}

<table class='table'>
	{{ AdminElements::tabulateUserHeaders() }}
	{{ AdminElements::tabulateUsers("info", "<span class='glyphicon glyphicon-pencil'></span>", "<span class='glyphicon glyphicon-remove'></span>") }}
</table>
{{ AdminElements::paginationLinks('formButtonWrapper') }}