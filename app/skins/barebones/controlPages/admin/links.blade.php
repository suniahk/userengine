@section( 'pageName' )
Links
@stop

<div class="page-header">
	<h1>Menu Links</h1>
</div>

{{ AdminElements::responseReport($errors) }}

{{ AdminElements::roleErrors( "alert alert-danger" ) }}

{{ AdminElements::newForm('form') }}
<table class='table'>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Priority</th>
		<th>Edit</th>
	</tr>
	@foreach (Link::all() as $link)
		<tr>
			<td>{{$link->name}}</td>
			<td>{{$link->type}}</td> <!-- TODO: Add support for type -->
			<td>{{AdminElements::placeholder()}}</td> <!-- TODO: Move Menu Priority from Plugins to links -->
			<td>@if($link->type!='plugin') <a href=' {{URL::to( 'controlPanel/admin/links/editLink/'.$link->id ) }} ."' class='btn'>Edit</a> @endif</td> <!-- TODO: Make Edit Page for non-plugin types -->
		</tr>
	@endforeach
</table>
{{ AdminElements::endForm() }}