@section( 'pageName' )
Create Role
@stop

<div class="page-header">
	<h1>Create a New User Role</h1>
</div>

{{ AdminElements::newForm('form') }}
	{{ AdminElements::responseReport($errors) }}	
	
	<div class="panel panel-default">
 		<div class="panel-heading">Basic Role Information</div>
		<div class="input-group">
			{{ AdminElements::roleName('input-group-addon', 'form-control') }}
		</div>
	</div>
	<div class="panel panel-default">
 		<div class="panel-heading">Role Permissions</div>
		{{ AdminElements::rolePermissions('input-group', 'form-control', 'input-group-addon') }}
	</div>

		<div class='formButtonWrapper'>
			{{ AdminElements::submitButton( 'btn btn-primary' , "Create Role" ) }}
		</div>
	</div>
{{ AdminElements::endForm() }}