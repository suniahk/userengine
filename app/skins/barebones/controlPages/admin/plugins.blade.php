@section( 'pageName' )
Plugins
@stop

<div class="page-header">
	<h1>Plugins {{ AdminElements::pluginCountBadge() }}</h1>
</div>

{{ AdminElements::responseReport($errors) }}

{{ AdminElements::pluginErrors("danger") }}
<table class='table'>
	{{ AdminElements::tabulatePluginHeaders() }}
	{{ AdminElements::tabulatePluginList("alert alert-danger") }}
</table>