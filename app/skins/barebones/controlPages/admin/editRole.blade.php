@section( 'pageName' )
Create Role
@stop

<div class="page-header">
	<h1>Edit User Role</h1>
</div>

{{ AdminElements::newForm('form') }}
	{{ AdminElements::responseReport($errors) }}	

	<div class="panel panel-default">
 		<div class="panel-heading">Basic Role Information</div>
		<div class="input-group">
			{{ AdminElements::roleName('input-group-addon', 'form-control', $role->name) }}
		</div>
	</div>
	<div class="panel panel-default">
 		<div class="panel-heading">Role Permissions</div>
		{{ AdminElements::rolePermissions('input-group', 'form-control', 'input-group-addon' , $role->id) }}
	</div>
	<div class="panel panel-default">
 		<div class="panel-heading">Plugin Permissions</div>
		{{ AdminElements::rolePluginPermissions('input-group', 'form-control', 'input-group-addon' , $role->id) }}
	</div>

		<div class='formButtonWrapper'>
			{{ AdminElements::submitButton( 'btn btn-primary' , "Save Role" ) }}
		</div>
	</div>
{{ Form::close() }}