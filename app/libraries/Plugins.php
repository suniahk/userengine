<?php

use Illuminate\Filesystem\Filesystem;

class Plugins {
	public static function files() {
		$fs = new Filesystem();
		$dirs = $fs->directories('./app/plugins');
		$plugins = [];

		foreach($dirs as $dir) {
			$plugins[] = $dir . "/plugin.xml";
		}
		return $plugins;
	}

	public static function checkSavedPlugins(){
		// First check to see if there are any non-existant entries.
		foreach(Plugin::all() as $plugin) {
			$remove = true;
			foreach(Plugins::files() as $file) {
				$view = simplexml_load_file($file);
				if($plugin->machineName == $view->machineName) {
					$remove = false;
				}
			}
			if ($remove) {
				Plugin::destroy($plugin->id);
			}
		}
		// Then check to see if there are any new plugins.
		$numPlugins = count(Plugin::all());
		if($numPlugins!=count(Plugins::files())){
			foreach(Plugins::files() as $file) {
				$view = simplexml_load_file($file);
				$name = $view->machineName;
	
				$rules = array( 'machineName' => 'unique:plugins,machineName' );
	
				$entry= array(
					'machineName' => $name,
				);
	
				$validator = Validator::make($entry, $rules);
	
				if ($validator->fails()) {
	
				} else {
					$plugin = new Plugin();
	
					// Save the new plugin
					$plugin->name=$view->displayName;
					$plugin->active=false;
					$plugin->machineName= $name;
					$plugin->tableName= $view->tableName;
					$plugin->author= $view->author;
					$plugin->description = $view->description;
					$plugin->hasAdmin = Plugins::checkForAdminPage($name);
					$plugin->hasView = Plugins::checkForPublicPage($name);
					$plugin->hasSettings = Plugins::checkForSettingsPage($name);
	
					$plugin->save();

					if (Permission::where('description', '=', "Plugin: ".$plugin->name)->count()==0) {
						$roleList = "";
						for ($i=1; $i<=Role::all()->count();$i++) {
							$roleList .= $i;
							if ($i != Role::all()->count()) {
								$roleList .= ",";
							}
						}

						$permission = new Permission();

						$permission->description = "Plugin: ".$plugin->name;
						$permission->roles = $roleList;
						$permission->linkedPlugin = $plugin->id;

						$permission->save();
					}
				}
			}
		}
	}

	public static function createPluginTable($name, $tableName) {
		$class='Plugins\\'.$name."\PluginController";
		if (method_exists($class, 'createDatabase')) {
			if (!Schema::hasTable($tableName)) {
				$class::createDatabase();
				return 1;
			}
		}

		return 0;
	}

	public static function checkForPublicPage($name) {
		$class='Plugins\\'.$name."\PluginController";
		if (method_exists($class, 'mainController')) {
			return 1;
		}

		return 0;
	}

	public static function checkForAdminPage($name) {
		$class='Plugins\\'.$name."\PluginController";
		if (method_exists($class, 'adminController')) {
			return 1;
		}

		return 0;
	}

	public static function checkForSettingsPage($name) {
		$class='Plugins\\'.$name."\PluginController";
		if (method_exists($class, 'settingsController')) {
			return 1;
		}

		return 0;
	}

	public static function getSettingMenuItems() {
		$menuItems = array();
		$query=Plugin::OrderBy('id')->where('hasSettings', '=', true)->get();

		foreach($query as $plugin) {
			$menuItems[] = array(
				'machineName' => $plugin->machineName,
				'name' => $plugin->name,
			);
		}

		return $menuItems;
	}
}