<?php

class BasicSkinElements {
	public static function newForm($form) {
		return Form::open(array('class' => $form ) );
	}

	public static function responseReport($errors) {
		$string = "";
		if($errors->has()) {
			$string.= '<div class="alert alert-danger">
				<p>There were problems with your information.  Please fix them before resubmitting.
				<ul>';
				foreach($errors->all() as $error) {
					$string.= '<li>'.$error.'</li>';
				}
				$string.= '</ul>
			</div>';
		}

		if(Session::get('success')!=null) {

			$string.= '<div class="alert alert-success">';
			$string.= Session::get('success');
			$string.= '</div>';
		}

		return $string;
	}

	public static function endForm() {
		return Form::close();
	}
}