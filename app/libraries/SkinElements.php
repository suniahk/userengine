<?php

class SkinElements extends BasicSkinElements {
	private static $currentUser;

	public static function setUser($user) {
		self::$currentUser = $user;
	}

	public static function isUserSelf() {
		if ( self::$currentUser->id == Auth::user()->id) {
			return 1;
		}
		return 0;
	}

	public static function currentIdIfAdmin () {
		if (UserEngine::checkRole(1) && !SkinElements::isUserSelf()) {
			return self::$currentUser->id;
		}

		return "";
	}

	public static function currentUsernameIfAdmin () {
		if (UserEngine::checkRole(1) && !SkinElements::isUserSelf()) {
			return "| ".self::$currentUser->username;
		}

		return "";
	}

	public static function submitButton($button, $addUser = false, $submitText = "Submit!") {
		if ($addUser) {
			return Form::hidden('user', self::$currentUser->id) . 
				Form::submit($submitText , array( 'class' => $button ) );
		} else {
			return Form::submit($submitText , array( 'class' => $button ) );
		}
	}

	public static function verifyPassword($label, $input) {
		return Form::label('password', 'Password' , array( 'class' => $label ) ) .
			Form::password('password' , array( 'class' => $input ) );
	}

	public static function listRoles($div, $span, $label) {
		$string = '';
		foreach(Role::all() as $role) {
			$string .='<div class="'.$div.'">';
				$string .= Form::label($role->id, $role->name , array( 'class' => $label ) );
				$string .= "<span class='" . $span . "'>";
					if(UserEngine::checkRole($role->id, self::$currentUser)) {
						$string .= Form::checkbox('role[]', $role->id, true );
					} else {
						$string .= Form::checkbox('role[]', $role->id );
					}
					
				$string .= '</span>
			</div>';
		}

		return $string;
	}

	public static function realName($label, $input) {
		return Form::label('name', 'Real Name' , array( 'class' => $label ) ) .
			Form::text('name', self::$currentUser->name, array( 'class' => $input ) );
	}

	public static function changeEmail($label, $input) {
		return Form::label('email', 'Email Address' , array( 'class' => $label ) ) .
			Form::text('email', self::$currentUser->email, array( 'class' => $input ) );
	}

	public static function newPassword($label, $input) {
		return Form::label('newPassword', 'New Password' , array( 'class' => 'input-group-addon' ) ) .
			Form::password('newPassword', array( 'class' => 'form-control' ) );
	}

	public static function confirmNewPassword($label, $input) {
		return Form::label('newPasswordConfirm', 'Confirm New Password' , array( 'class' => $label ) ) .
			Form::password('newPasswordConfirm', array( 'class' => $input ) );
	}
}