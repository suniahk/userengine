<?php

use Illuminate\Filesystem\Filesystem;

class Skinning {
	public static function files() {
		$fs = new Filesystem();
		$dirs = $fs->directories('./app/skins');
		$skins = [];

		foreach($dirs as $dir) {
			$skins[] = $dir . "/skin.xml";
		}
		return $skins;
	}

	public static function checkSavedSkins(){
		// First check to see if there are any non-existant entries.
		foreach(Skin::all() as $skin) {
			$remove = true;
			foreach(Skinning::files() as $file) {
				$view = simplexml_load_file($file);
				if($skin->machineName == $view->machineName) {
					$remove = false;
				}
			}
			if ($remove) {
				Skin::destroy($skin->id);
			}
		}
		// Then check to see if there are any new skins.
		$priority = count(Skin::all());
		if($priority!=count(Skinning::files())){
			foreach(Skinning::files() as $file) {
				$view = simplexml_load_file($file);
				$name = $view->machineName;
	
				$rules = array( 'machineName' => 'unique:skins,machineName' );
	
				$entry= array(
					'machineName' => $name,
				);
	
				$validator = Validator::make($entry, $rules);
	
				if ($validator->fails()) {
	
				} else {
					$priority++;
					$skin = new Skin();
	
					$skin->name=$view->name;
					$skin->active=false;
					$skin->machineName= $name;
					$skin->author= $view->author;
					$skin->description = $view->description;
					$skin->screenshot = $view->screenshot;
	
					$skin->save();
				}
			}
		}
	}
}