<?php

class Links {
	public static function realignPriorities () {
		$fixedPriority = 0;
		foreach(Link::orderBy('priority')->get() as $link) {
			$fixedPriority++;

			$link->priority = $fixedPriority;
			$link->save();
		}
	}

	public static function getMenuItems($where='all') { // TODO: Fix Priority... Maybe a seperate admin menu?
		$menuItems = array();
		$query=Link::OrderBy('id')->where('location', '=', $where)->get();

		foreach($query as $link) {
			$menuItems[] = array(
				'url' => $link->url,
				'name' => $link->name,
				'machineName' => $link->machineName,
			);
		}

		return $menuItems;
	}


	public static function changePriorityUp($which) {
		$link = Link::find($which);

		if($link->priority!=1) { //Make sure we're not trying to promote the highest link
			$affectedLink = Link::where('priority', '=', $link->priority-1)->first();

			$affectedLink->priority +=1;
			$link->priority -=1;

			$link->save();
			$affectedLink->save();
		}
	}
	public static function changePriorityDown($which) {
		$link = Link::find($which);

		if($link->priority!=count(Plugin::all())) { //Make sure we're not trying to demote the lowest link
			$affectedLink = Link::where('priority', '=', $link->priority+1)->first();

			$affectedLink->priority -=1;
			$link->priority +=1;

			$link->save();
			$affectedLink->save();
		}
	}
}