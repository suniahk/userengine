<?php

class AdminElements extends BasicSkinElements {
	private $currentUser;

	public static function submitButton($button, $submitText = "Submit!") {
			return Form::submit($submitText , array( 'class' => $button ) );
	}

	public static function changeSiteName($label, $input) {
		return Form::label('name', 'Website Name' , array( 'class' => $label ) ) .
			Form::text('name', SiteSetting::where('key', '=', 'siteName')->first()->value, array( 'class' => $input ) );
	}

	public static function changeSiteSkin($label, $input) {
		$results=[];
		foreach(Skin::all() as $skin); {
			$results[$skin->id]=$skin->name;
		}
		return Form::label('skin', 'Website Skin' , array( 'class' => $label ) ) .
			Form::select('skin', $results, SiteSetting::where('key', '=', 'siteSkin')->first()->value, array( 'class' => $input ) );
	}

	public static function roleName ($label, $input, $role = null) {
		if ($role == null) {
			$role = Input::old('name');
		}
		return Form::label('name', 'Role Name' , array( 'class' => $label ) ) .
			Form::text('name', $role, array( 'class' => $input ) );
	}

	public static function rolePermissions($div, $span, $label, $role = null) {
		if ($role == null) {
			$role = Role::where('default', '=', true)->first()->id;
		}

		$string = '';

		foreach(Permission::where('linkedPlugin', '=', '0')->get() as $permission) {
			$string.= '<div class="'.$div.'">';
			$string.=  Form::label($permission->id, $permission->description , array( 'class' => $label ) );
			$string.= "<span class='".$span."'>";
				if(Permissions::roleHasPermission($role, $permission->id)) {
					$string.= Form::checkbox('permissions[]', $permission->id, true );
				} else {
					$string.= Form::checkbox('permissions[]', $permission->id );
				}
				
			$string.= "</span>
			</div>";
		}

		return $string;
	}

	public static function rolePluginPermissions($div, $span, $label, $role = null) {
		if ($role == null) {
			$role = Role::where('default', '=', true)->first()->id;
		}

		$string = '';

		foreach(Permission::where('linkedPlugin', '!=', '0')->get() as $permission) {
			if (Plugin::find($permission->linkedPlugin)->active) {
				$string.= '<div class="'.$div.'">';
				$string.=  Form::label($permission->id, $permission->description , array( 'class' => $label ) );
				$string.= "<span class='".$span."'>";
					if(Permissions::roleHasPermission($role, $permission->id)) {
						$string.= Form::checkbox('permissions[]', $permission->id, true );
					} else {
						$string.= Form::checkbox('permissions[]', $permission->id );
					}
					
				$string.= "</span>
				</div>";
			}
		}

		return $string;
	}

	public static function tabulatePluginHeaders($rowClass="", $headerClass="") {
		return "<tr class='" .$rowClass. "''>
		<th class='" .$headerClass. "''>Plugin Name</th>
		<th class='" .$headerClass. "''>Author</th>
		<th class='" .$headerClass. "''>Description</th>
		<th class='" .$headerClass. "''>Enable</th>
		<th class='" .$headerClass. "''>Disable</th>
		<th class='" .$headerClass. "''>Admin</th>
	</tr>";
	}

	public static function tabulatePluginList($disabledPluginClass, $tdClass='') {
		$string = "";
		foreach(Plugin::orderBy('id')->get() as $plugin) {
			$string .= "<tr ";
			if(!$plugin->active) 
				$string .= "class='".$disabledPluginClass."'";
			$string.=">
			<td class='" .$tdClass. "'>" . $plugin->name ."</td>
			<td class='" .$tdClass. "'>" . $plugin->author ."</td>
			<td class='" .$tdClass. "'>" . $plugin->description ."</td>
			<td class='" .$tdClass. "'>";
			if(!$plugin->active)
				$string.= "<a href='" . URL::to( 'controlPanel/admin/twiddlePlugin/'.$plugin->id ) . "' class='btn btn-success btn-sm'>Enable</a>";
			$string.= "</td>
			<td class='" .$tdClass. "'>";
			if($plugin->active) {
				$string.= "<a href='" . URL::to( 'controlPanel/admin/twiddlePlugin/'.$plugin->id ) . "' class='btn btn-danger btn-sm'>Disable</a></td>";
				if (Plugins::checkForAdminPage($plugin->machineName))
				$string.= "<td class='" .$tdClass. "'><a href='" . URL::to( 'controlPanel/admin/plugin/'.$plugin->id ) . "' class='btn btn-info btn-sm'>Admin</a>";
			} else {
				$string .= "</td><td>";
			}
			$string.= "</td>
		</tr>";
		}

		return $string;
	}

	public static function placeholder(){
		$string='';
		foreach(Link::orderBy('id')->get() as $link) {
			if($link->priority!=1)
				$string.= "<a href='" . URL::to( 'controlPanel/admin/promote/'.$link->id ) ."'><span class='glyphicon glyphicon-arrow-up'></a></span>";
			if($link->priority!=Link::all()->count())
				$string.= "<a href='" . URL::to( 'controlPanel/admin/demote/'.$link->id ) . "'><span class='glyphicon glyphicon-arrow-down'></a></span>";
		}
		return $string;
	}

	public static function pluginErrors($danger) {
		$string = '';
		if(count(Plugins::files())==0) {
			$string.="<div class='".$danger."'>
				You currently don't have any plugins installed.
			</div>";
		}

		return $string;
	}

	public static function pluginCountBadge($type="badge") {
		return "<span class='".$type."'>" . count(Plugin::all()) . "</span>";
	}

	public static function newRoleButton($linkContents, $linkClass="") {
		return "<a href='" . URL::to( 'controlPanel/admin/roles/addRole' ) . "' class='" . $linkClass . "'>" . $linkContents . "</a>";
	}

	public static function roleErrors ($noRoles="") {
		$string = "";
		if(count(Role::all())==0) {
		$string.="<div class='".$noRoles."'>
			You don't have any roles defined.
		</div>";
		}

		return $string;
	}

	public static function tabulateRoleHeaders ($rowClass="", $headerClass="") {
		return "<tr class='" .$rowClass. "''>
			<th class='" .$headerClass. "''>Role Name</th>
			<th class='" .$headerClass. "''>Actions</th>
		</tr>";
	}

	public static function tabulateUserRoles ($buttons, $delete, $badge="badge") {
		$string = "";
		foreach(Role::all() as $role) {
			$string.="<tr>
				<td>" . $role->name;
				if($role->default)
					$string .=" <span class='". $badge ."'>Default Role</span>";
				$string .="</td>
				<td><a href='" . URL::to( 'controlPanel/admin/roles/editRole/'.$role->id ) ."' class='" . $buttons . "'>Edit</a> ";
				if(!$role->default)
					$string .="<a href='" . URL::to( 'controlPanel/admin/roles/makeDefault/'.$role->id ) . "' class='" . $buttons . "'>Make Default</a>";
				if(!$role->special)
					$string .="<a href='" . URL::to( 'controlPanel/admin/roles/confirmDelete/'.$role->id ) . "' class='" . $delete . "'>Delete</a>";
				$string .="</td>
			</tr>";
		}

		return $string;
	}

	public static function confirmDeleteUser($warningBox="", $confirm="", $cancel="") {
		$string = "";
		if(Session::get('confirmDelete')!=null) {
		$string .="<div class='".$warningBox."'>
		Are you sure you want to delete " . Session::get('confirmDelete')['user'] . "?  This will completely remove the account from the system and can not be undone.
		<br />
		<br />
		<a href='". URL::to( 'controlPanel/admin/removeUser/'.Session::get('confirmDelete')['id'] ) ."' class='".$confirm."'>I&apos;m sure, proceed!</a>
		<a href='". URL::to( 'controlPanel/admin/userMgmt' ) ."' class='".$cancel."'>On second thought...</a>
	</div>";
		}
		return $string;
	}

	public static function paginationLinks($class='', $perPage=25) {
		return "<div class='".$class."'>". User::paginate($perPage)->links() ."</div>";
	}

	public static function tabulateUserHeaders ($rowClass="", $headerClass="") {
		return "<tr class='" .$rowClass. "''>
		<th class='" .$headerClass. "''>Name</th>
		<th class='" .$headerClass. "''>Username</th>
		<th class='" .$headerClass. "''>Email Address</th>
		<th class='" .$headerClass. "''>Roles</th>
		<th class='" .$headerClass. "''>Actions</th>
	</tr>";
	}

	public static function tabulateUsers($onDelete, $editButton, $deleteButton, $perPage=25) {
		$string="";

		foreach(User::paginate($perPage) as $user) {
		$string.="<tr ";
		if(Session::get('confirmDelete')!=null) {
			if(Session::get('confirmDelete')['id']==$user->id) {
				$string.="class='info'";
			}
		}
		$string.=">
		<td>". $user->name ."</td>
		<td>". $user->username ."</td>
		<td>". $user->email ."</td>
		<td>". UserEngine::outputRoles($user->roles) ."</td>
		<td>
			<a href='". URL::to( 'controlPanel/settings/basic/'.$user->id ) ."'>".$editButton."</a>
			<a href='". URL::to( 'controlPanel/admin/confirmDelete/'.$user->id ) ."'>".$deleteButton."</a>
		</td>
	</tr>";
		}
		return $string;
	}
}