<?php

class Permissions {
	public static function checkPermissions($permission, $user=null) {
		if ($user == null) {
			$user = Auth::user();
		}
		$userRoles = explode(',', $user['roles']);
		$permissionRoles = explode(',', Permission::find($permission)['roles']);

		foreach($userRoles as $role) {
			foreach($permissionRoles as $pRole) {
				if($role == $pRole) {
					return true;
				}
			}
		}

		return false;
	}

	public static function roleHasPermission($role, $permission) {
		$permissionRoles = explode(',', Permission::find($permission)['roles']);

		foreach($permissionRoles as $pRole) {
			if($role == $pRole) {
				return true;
			}
		}

		return false;
	}
}