<?php

class UserEngine {
	public static function storeUserRoles() {
		$string = '';
		$counter = 0;
		foreach(Input::get('role') as $role) {
			$counter++;
			$string.=$role;
			if($counter!=count(Input::get('role'))) {
				$string.=',';
			}
		}
		if(Input::get('user')!== null) {
			$user = User::find(Input::get('user'));
		} else {
			$user = User::find(Auth::user()->id);
		}
		

		$user->roles=$string;

		$user->save();
		return Redirect::back()->with('success', 'The user&apos;s roles have been updated successfully.');
	}

	public static function storeSiteSettings() {
		$name = SiteSetting::where('key', '=', 'siteName')->first();
		$skin = SiteSetting::where('key', '=', 'siteSkin')->first();

		if(Input::get('name')!== null) {
			$name->value=Input::get('name');
			$name->save();
		}

		$skin->value=Input::get('skin');
		$skin->save();

		return Redirect::back()->with('success', 'Your site settings have been updated successfully.');
	}

	public static function storeRolePermissions($role) {
		foreach(Permission::all() as $permission) {
			$activeRole = 1;
			if ($permission->linkedPlugin>0) {
				$activeRole = Plugin::find($permission->linkedPlugin)->active;
			}

			if ($activeRole){
				if($permission->roles!='') {
					$roles = explode(',', $permission->roles);
				} else {
					$roles = [];
				}
				$permissionExists = false;

				for($i=0;$i<count(Input::get('permissions'));$i++) {
					if(Input::get('permissions')[$i]==$permission->id) {
						$permissionExists = true;
					}
				}

				if($permissionExists) {
					if(!in_array($role, $roles)) {
						$roles[] = $role;
					}
				} else {
					if(in_array($role, $roles)) {
						$rolePosition = 0;
						for ($r = 0; $r<count($roles);$r++) {
							if ($roles[$r] == $role) {
								$rolePosition = $r;
							}
						}
						unset($roles[$rolePosition]);
					}
				}

				$roles=array_values($roles);

				$string='';

				for($i=0;$i<count($roles);$i++) {
					$string.=$roles[$i];

					if($i+1!=(count($roles))) {
						$string.=',';
					}
				}

				$permission->roles = $string;

				$permission->save();
			}
		}
	}

	public static function checkRole($needle, $user=null) {
		if ($user == null) {
			$user = Auth::user();
		}
		$roles = explode(',', $user->roles);
		foreach($roles as $role) {
			if($role == $needle) {
				return true;
			}
		}
		return false;
	}

	public static function checkAdminAccess($page) {
		if($page=='admin' && UserEngine::checkRole(0)) {
			return true;
		}

		return false;
	}

	public static function UsersWithRole($needle) {
		$users=[];

		foreach(User::all() as $user) {
			if(UserEngine::checkRole($needle, $user)) {
				$users[]=$user;
			}
		}

		return $users;
	}

	public static function getRoleNames($userRoles) {
		$numericalRoles = explode(',', $userRoles);
		$roles = [];

		foreach($numericalRoles as $role) {
			$roles[] = Role::find($role)['name'];
		}

		return $roles;
	}

	public static function outputRoles($roles) {
		$rolesToOutput=UserEngine::getRoleNames($roles);
		$string = '';
		$counter=0;

		foreach($rolesToOutput as $role) {
			$counter++;
			$string .= $role;

			if ($counter != count($rolesToOutput)) {
				$string .= ', ';
			}
		}

		return $string;
	}

	public static function processSocialSettingsChange() {
		$rules = array(
			'password'			=>	'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::back()
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			$userdata = array(
				'username' 	=> Auth::user()->username,
				'password' 	=> Input::get('password')
			);

			if ( Auth::attempt( $userdata ) ) {
				if (UserEngine::checkRole('ADMIN')) {
					$userId=Input::get('user');
				} else {
					$userId=Auth::user()->id;
				}
				$user = User::find($userId);
				$user->name = Input::get( 'name' );

				$user->save();

				if (UserEngine::checkRole('ADMIN') && $userId!=Auth::user()->id) {
					return Redirect::to( 'controlPanel/admin/userMgmt' )->with('success', 'The user&apos;s profile was updated successfully!');;
				}

				return Redirect::back()->with('success', 'Your profile was updated successfully!');
			} else {
				$errors = array(
					'auth' => 'The password you entered is incorrect.'
				);
				return Redirect::back()->withErrors( $errors );
			}
		}
	}

	public static function processRegistration() {
		$rules = array(
			'email'				=>	'email|required',
			'password'			=>	'required',
			'passwordConfirm'	=>	'required_with:password|same:password',
			'username'			=>	'required|unique:users,username'
		);

		$messages = array(
		    'passwordConfirm.same' => 'Your passwords don&apos;t match.',
		    'passwordConfirm.required_with' => 'You need to confirm your password!'
		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails()) {
			return Redirect::to('register')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password', 'passwordConfirm')); // send back the input (not the password) so that we can repopulate the form
		} else {
			$user = new User();
			$user->name = "";
			$user->username = Input::get('username');
			$user->email = Input::get('email');
			$user->password = Hash::make( Input::get('password') );
			$user->roles = '['.Role::where('default', '=', 1)->first()->id.']';

			$user->save();

			return Redirect::to( '/' )->with('success', 'Your registration was completed successfully!  Please log in.');
		}
	}

	public static function processSiteSettingsChange() {
		$siteName = SiteSetting::where('key', '=', 'siteName')->first();

		$siteName->value=Input::get('name');

		$siteName->save();

		return Redirect::back()->with('success', 'Your settings have been updated successfully.');
	}

	public static function processBasicSettingsChange() {
		$rules = array(
			'email'    => 'email',
			'password' => 'required', // password can only be alphanumeric and has to be greater than 3 characters
			'newPasswordConfirm' => 'required_with:newPassword|same:newPassword'
		);

		$messages = array(
		    'newPasswordConfirm.same' => 'Your new passwords don&apos;t match.',
		    'newPasswordConfirm.required_with' => 'You need to confirm your password!'
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails()) {
			return Redirect::back()
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::only('email')); // send back the input (not the password) so that we can repopulate the form
		} else {
			$userdata = array(
				'username' 	=> Auth::user()->username,
				'password' 	=> Input::get('password')
			);

			if ( Auth::attempt( $userdata ) ) {
				if (UserEngine::checkRole('ADMIN')) {
					$userId=Input::get('user');
				} else {
					$userId=Auth::user()->id;
				}

				$user=User::find($userId);
				if ( Input::get( 'email' ) !== '' ) {
					$user->email = Input::get( 'email' );
				}

				if ( Input::get( 'newPassword' ) !== '' ) {
					$user->password = Hash::make( Input::get( 'newPassword' ) );
				}

				$user->save();

				if (UserEngine::checkRole('ADMIN') && $userId!=Auth::user()->id) {
					return Redirect::to( 'controlPanel/admin/userMgmt' )->with('success', 'The user&apos;s profile was updated successfully!');;
				}

				return Redirect::back()->with('success', 'Your profile was updated successfully!');
			} else {
				$errors = array(
					'auth' => 'The password you entered is incorrect.'
				);
				return Redirect::back()->withErrors( $errors );
			}
		}

		return Redirect::back();
	}
}