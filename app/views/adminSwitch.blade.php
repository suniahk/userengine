@include( 'menus.adminMenu' , array( 'ul'=>'nav nav-pills' ) )

@if ($subPage == 'plugin')
	@include($pluginPage)
@else
	@include('display::controlPages.admin.'.$subPage)
@endif