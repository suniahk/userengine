<!doctype html>
<html>
	<head>
		<title>@yield( 'pageName' )
		- {{ SiteSetting::where('key', '=', 'siteName')->first()->value }}</title>
		{{ HTML::style( 'bootstrap/css/bootstrap.css' ) }}
		{{ HTML::style( 'css/basic.css' ) }}
	</head>
	<body>
		<div class='wrapper'>
			@yield( 'content' )
		</div>

		{{ HTML::script( 'js/jquery-2.1.0.min.js' ) }}
		{{ HTML::script( 'bootstrap/js/bootstrap.min.js' ) }}
	</body>
</html>