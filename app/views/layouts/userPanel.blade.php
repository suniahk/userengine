@extends( 'layouts.baselayout' )

@section( 'content' )
	<div class='linkBar'>
		@yield( 'linkBar' )
	</div>
	<div class='contents'>
		@yield( 'page' )
	</div>
@stop