@extends( 'layouts.baselayout' )

@section( 'pageName' )
Login
@stop

@section( 'content' )
	<div class='contents'>
	@include( 'forms.loginForm' )
	</div>
@stop