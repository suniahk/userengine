@extends( 'layouts.userPanel' )

@section( 'linkBar' )
<!--TODO: Clean up linkBar for skins -->
<ul style="width: 100%;" class="nav nav-tabs">
	<li @if ($page=='display::controlPages.dashboard') class="active" @endif><a href="{{ URL::to('controlPanel') }}">Dashboard</a></li>
	@foreach(Links::getMenuItems('main') as $item)
	@if(Permissions::checkPermissions(Permission::where('linkedPlugin', '=', Plugin::where('machineName', '=', $item['machineName'])->firstOrFail()->id)->firstOrFail()->id))<li @if ($page=='plugins::'.$item['machineName']) class="active" @endif><a href="{{ URL::to($item['url']) }}">{{ $item['name'] }}</a></li>@endif
	@endforeach
	<li class="dropdown pull-right @if($page=='display::controlPages.admin' || $page=='display::controlPages.settings') active @endif">
	    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="cursor:pointer">
	    	@if($page=='display::controlPages.admin')
	    		Administration
	    	@elseif($page=='display::controlPages.settings')
	    		Settings
	    	@else
	    		<span class='glyphicon glyphicon-user'></span> {{ Auth::user()->username }}
	    	@endif <span class="caret"></span>
	    </a>
		<ul class="dropdown-menu pull-right">
			@foreach(Links::getMenuItems('side') as $item)
			<li @if ($page=='display::plugins.'.$item['url']) class="active" @endif><a href="{{ URL::to($item['url']) }}">{{ $item['name'] }}</a></li>
			@endforeach
			<li><a href="{{ URL::to('controlPanel/settings') }}">Settings</a></li>
			@if ( UserEngine::checkRole( SiteSetting::returnValue('adminRole') ) )
				<li><a href="{{ URL::to('controlPanel/admin') }}">Administration</a></li>
			@endif
			<li class='divider'></li>
			<li><a href="{{ URL::to('logout') }}">Logout</a></li>
		</ul>
	</li>
</ul>
@stop

@section( 'page' )
	@include($page)
@stop