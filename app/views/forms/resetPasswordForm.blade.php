{{ Form::open(array('action' => 'RemindersController@postReset')) }}
	@if($errors->count()>0)
	<div class="alert alert-danger">
		<p>You have one or more errors with your reset form.  Please fix them before resubmitting.
		<ul>
		@if($errors->first('password')!=null)<li>{{ $errors->first('password') }}</li>@endif
		@if($errors->first('email')!=null)<li>{{ $errors->first('email') }}</li>@endif
		@if($errors->first('password_confirmation')!=null)<li>{{ $errors->first('passwordConfirm') }}</li>@endif
		</ul>
	</div>
	@endif

	<div class="panel panel-default">
 		<div class="panel-heading">Reset Password</div>
 		<div class="panel-body">All fields are mandatory.</div>

 			{{ Form::hidden('token', $token) }}

		<div class="input-group">
			{{ Form::label('email', 'Email Address' , array( 'class' => 'input-group-addon' ) ) }}
			{{ Form::text('email', Input::old('email'), array( 'class' => 'form-control' ) ) }}
		</div>

		<div class="input-group">
			{{ Form::label('password', 'Password' , array( 'class' => 'input-group-addon' ) ) }}
			{{ Form::password('password' , array( 'class' => 'form-control' ) ) }}
		</div>
		<div class="input-group">
			{{ Form::label('password_confirmation', 'Confirm Password' , array( 'class' => 'input-group-addon' ) ) }}
			{{ Form::password('password_confirmation' , array( 'class' => 'form-control' ) ) }}
		</div>

		<div class='formButtonWrapper'>
		{{ Form::submit('Submit' , array( 'class' => 'btn btn-primary' ) ) }}
		</div>
	</div>
{{ Form::close() }}