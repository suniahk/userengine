{{ Form::open(array('url' => 'register')) }}
	@if($errors->count()>0)
	<div class="alert alert-danger">
		<p>You have one or more errors with your registration form.  Please fix them before resubmitting.
		<ul>
		@if($errors->first('username')!=null)<li>{{ $errors->first('username') }}</li>@endif
		@if($errors->first('password')!=null)<li>{{ $errors->first('password') }}</li>@endif
		@if($errors->first('email')!=null)<li>{{ $errors->first('email') }}</li>@endif
		@if($errors->first('passwordConfirm')!=null)<li>{{ $errors->first('passwordConfirm') }}</li>@endif
		</ul>
	</div>
	@endif

	<div class="panel panel-default">
 		<div class="panel-heading">Registration</div>
 		<div class="panel-body">All fields are mandatory.</div>

		<div class="input-group">
			{{ Form::label('username', 'User' , array( 'class' => 'input-group-addon' ) ) }}
			{{ Form::text('username', Input::old('username'), array( 'class' => 'form-control' ) ) }}
		</div>

		<div class="input-group">
			{{ Form::label('email', 'Email Address' , array( 'class' => 'input-group-addon' ) ) }}
			{{ Form::text('email', Input::old('email'), array( 'class' => 'form-control' ) ) }}
		</div>

		<div class="input-group">
			{{ Form::label('password', 'Password' , array( 'class' => 'input-group-addon' ) ) }}
			{{ Form::password('password' , array( 'class' => 'form-control' ) ) }}
		</div>
		<div class="input-group">
			{{ Form::label('passwordConfirm', 'Confirm Password' , array( 'class' => 'input-group-addon' ) ) }}
			{{ Form::password('passwordConfirm' , array( 'class' => 'form-control' ) ) }}
		</div>

		<div class='formButtonWrapper'>
		{{ Form::submit('Submit Registration' , array( 'class' => 'btn btn-primary' ) ) }}
		<a href='{{ URL::to('/') }}' class="btn">Go Back</a>
		</div>
	</div>
{{ Form::close() }}