{{ Form::open(array('url' => 'resetPassword')) }}
	<div class="panel panel-default">
 		<div class="panel-heading">Forgot Password</div>
 		<div class="panel-body">
			@if($errors->count()>0)
			<div class="alert alert-danger">
				{{ $errors->first('email') }}
			</div>
			@endif

			@if(Session::get('status')!=null)
			<div class="alert alert-success">
				{{ Session::get('status') }}
			</div>
			@endif

			Please enter your email address.  If an account is found with that address, an email will be
 								sent to you with instructions to resent your password.

		</div>

		<div class="input-group">
			{{ Form::label('email', 'Email' , array( 'class' => 'input-group-addon' ) ) }}
			{{ Form::text('email', Input::old('email'), array( 'class' => 'form-control' ) ) }}
		</div>

		<div class='formButtonWrapper'>
			{{ Form::submit('Send Reminder' , array( 'class' => 'btn btn-primary' ) ) }}
			<a href='{{ URL::to('/') }}' class="btn">Go Back</a>
		</div>

	</div>
{{ Form::close() }}