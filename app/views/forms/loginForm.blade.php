{{ Form::open(array('url' => '/')) }}
	<!-- if there are login errors, show them here -->
	@if($errors->count()>0)
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			{{ $error }}
		@endforeach
	</div>
	@endif

	@if(Session::get('success')!=null)
	<div class="alert alert-success">
		{{ Session::get('success') }}
	</div>
	@endif

	<div class="input-group">
		{{ Form::label('username', 'User' , array( 'class' => 'input-group-addon' ) ) }}
		{{ Form::text('username', Input::old('username'), array( 'class' => 'form-control' ) ) }}
	</div>

	<div class="input-group">
		{{ Form::label('password', 'Password' , array( 'class' => 'input-group-addon' ) ) }}
		{{ Form::password('password' , array( 'class' => 'form-control' ) ) }}
	</div>

	<div class='formButtonWrapper'>
		{{ Form::submit('Login and Remember' , array( 'class' => 'btn btn-default btn-xs' ) ) }}
		{{ Form::submit('Login' , array( 'class' => 'btn btn-primary' ) ) }}
		<div class="btn-group btn-group-xs">
		  <a href='{{ URL::to('resetPassword') }}' class="btn btn-default">Forgot Password</a>
		  <a href='{{ URL::to('register') }}' class="btn btn-default">Register</a>
		</div>

	</div>
{{ Form::close() }}