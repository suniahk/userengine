<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset for {{ SiteSetting::where('key','=','siteName')->first()->value }}</h2>

		<div>
			Recently, someone (hopefully you) submitted a request to have their password reset.  If this request was sent by you, click <a href="{{ URL::to('password/reset', array($token)) }}">this<a> link and fill out the form.

			If this request wasn&apos;t sent by you, don&apos;t worry.  You can ignore this email and your account information will stay the same.

			Regards,
			The Email Generation Robot for {{ SiteSetting::where('key','=','siteName')->first()->value }}

			P.S. If the above link isn't working, copy and paste the following into your browser: {{ URL::to('password/reset', array($token)) }}
		</div>
	</body>
</html>