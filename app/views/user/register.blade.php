@extends( 'layouts.baselayout' )

@section( 'pageName' )
UserEngine Registration Page
@stop

@section( 'content' )
	<div class='contents'>
	@include( 'forms.registerForm' )
	</div>
@stop