@extends( 'layouts.baselayout' )

@section( 'title' )
UserEngine Password Reset Page
@stop

@section( 'content' )
	<div class='contents'>
	@include( 'forms.resetPasswordForm' )
	</div>
@stop