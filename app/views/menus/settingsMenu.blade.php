<ul class='{{ $ul }}'>
	@if(UserEngine::checkRole(1))
		<li @if ($subPage=='admin') class="active" @endif><a href="{{ URL::to( 'controlPanel/settings/admin/' . SkinElements::currentIdIfAdmin() ) }}">Administrative Settings</a></li>
	@endif
		<li @if ($subPage=='basic') class="active" @endif><a href="{{ URL::to( 'controlPanel/settings/basic/' . SkinElements::currentIdIfAdmin() ) }}">Basic Account Settings</a></li>
	<li @if ($subPage=='social') class="active" @endif><a href="{{ URL::to( 'controlPanel/settings/social/' . SkinElements::currentIdIfAdmin() ) }}">Social</a></li>
	@foreach (Plugins::getSettingMenuItems() as $item)
		<li @if ($subPage=='plugin') @if ($pluginPage=='plugins::'.$item['machineName']) class="active" @endif @endif><a href="{{ URL::to( 'controlPanel/settings/plugin/'.$item['machineName'] . "/" . SkinElements::currentIdIfAdmin() ) }}">{{ $item['name'] }}</a></li>
	@endforeach
</ul>