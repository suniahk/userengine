<ul class='{{ $ul }}'>
	@if(Permissions::checkPermissions(2))<li @if ($subPage=='info') class="active" @endif><a href="{{ URL::to( 'controlPanel/admin/info' ) }}">Site Info</a></li>@endif
	@if(Permissions::checkPermissions(3))<li @if ($subPage=='userMgmt') class="active" @endif><a href="{{ URL::to( 'controlPanel/admin/userMgmt' ) }}">User Management</a></li>@endif
	@if(Permissions::checkPermissions(5))<li @if ($subPage=='roles') class="active" @endif><a href="{{ URL::to( 'controlPanel/admin/roles' ) }}">User Roles</a></li>@endif
	@if(Permissions::checkPermissions(4))<li @if ($subPage=='plugins' || $subPage=='plugin') class="active" @endif><a href="{{ URL::to( 'controlPanel/admin/plugins' ) }}">Plugin Manager</a></li>@endif
	@if(Permissions::checkPermissions(7))<li @if ($subPage=='links') class="active" @endif><a href="{{ URL::to( 'controlPanel/admin/links' ) }}">Menu Links</a></li>@endif
</ul>