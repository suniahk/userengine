@include( 'menus.settingsMenu', array( 'ul' => "nav nav-pills" ) )

@if ($subPage == 'plugin')
	@include($pluginPage)
@else
	@include('display::controlPages.settings.'.$subPage)
@endif
